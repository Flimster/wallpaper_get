"""This script takes care of getting the top 100 wallpapers from r/MinimalWallpaper"""
import os
import json
from urllib.request import Request, urlopen, urlretrieve
from PIL import Image

request = Request("https://www.reddit.com/r/MinimalWallpaper/top/.json?limit=200&sort=top&t=all")
request.add_header("User-agent", "my-bot")
reddit_posts = json.loads(urlopen(request).read())

num_of_posts = reddit_posts["data"]["dist"]
print(num_of_posts)

for i in range(0, num_of_posts):
    try:
        file_url = reddit_posts["data"]["children"][i]["data"]["url"]
        if file_url[-4:] == ".png" or file_url[-4:] == ".jpg":
            local_filename, headers = urlretrieve(file_url, str(i) + file_url[-4:])
            print(f"Url is {file_url} and file saved is {local_filename}")
    except:
        print("Something went wrong")

print("Beginning validation of images")

for (pth, dirs, files) in os.walk("/Users/skoli/Documents/Self-learning/wallpaper_get/wallpapers"):
    for filename in files:
        with Image.open(os.path.join(pth, filename)) as image:
            width, height = image.size
            if width < 1920 or height < 1080:
                os.remove(os.path.join(pth, filename))
                print(f"Remoevd {filename}")

def main():
    print("Hello world")

if __name__ == "__main__":
    main()
